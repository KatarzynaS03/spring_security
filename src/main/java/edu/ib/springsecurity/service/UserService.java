package edu.ib.springsecurity.service;

import edu.ib.springsecurity.dto.UserDto;
import edu.ib.springsecurity.dto.UserDtoBuilder;
import edu.ib.springsecurity.repo.UserRepository;
import edu.ib.springsecurity.repo.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {


    private UserRepository customerRepository;

    @Autowired
    private UserDtoBuilder userDtoBuilder;

    @Autowired
    public UserService(UserRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public Optional<UserDto> FindById(Long id){
        return customerRepository.findById(id);
    }

    public UserDto findByName (String name){
        return customerRepository.findByName(name);
    }

    public Iterable<UserDto> findALL(){
        return customerRepository.findAll();
    }

    @EventListener(ApplicationReadyEvent.class)
    public void fillDB(){
        User us = new User("Mariola","Mariola123","ROLE_ADMIN");
        User uss = new User("Jola","Jola123","ROLE_CUSTOMER");
        customerRepository.save(userDtoBuilder.buildUser(us));
        customerRepository.save(userDtoBuilder.buildUser(uss));

    }

    public UserDto save(UserDto customer) {
        return customerRepository.save(customer);
    }
}
