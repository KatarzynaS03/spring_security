package edu.ib.springsecurity.service;

import edu.ib.springsecurity.repo.ProductRepository;
import edu.ib.springsecurity.repo.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProduktService {

    private final ProductRepository productRepository;

    @Autowired
    public ProduktService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public Optional<Product> FindById(Long id){
        return productRepository.findById(id);
    }

    public Iterable<Product> findALL(){
        return productRepository.findAll();
    }
    public Product Save(Product videoCassette){
        return productRepository.save(videoCassette);
    }

    public void delete(Long id){
        productRepository.deleteById(id);
    }

    @EventListener(ApplicationReadyEvent.class)
    public void fillDB(){
        Save(new Product(1L,"Honda papieska", 1000, true));
        Save(new Product(2L,"Toyota Avensis Verso", 1237, false));

    }

    public void save(Product product) {
        productRepository.save(product);
    }
}
