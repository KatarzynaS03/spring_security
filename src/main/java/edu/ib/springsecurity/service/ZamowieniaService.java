package edu.ib.springsecurity.service;

import edu.ib.springsecurity.repo.ZamowienieRepository;
import edu.ib.springsecurity.repo.entity.Customer;
import edu.ib.springsecurity.repo.entity.Product;
import edu.ib.springsecurity.repo.entity.Zamowienie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Optional;


@Service
public class ZamowieniaService {

    private final ZamowienieRepository orderRepository;

    @Autowired
    public ZamowieniaService(ZamowienieRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public Optional<Zamowienie> FindById(Long id){
        return orderRepository.findById(id);
    }

    public Iterable<Zamowienie> findALL(){
        return orderRepository.findAll();
    }
    public Zamowienie Save(Zamowienie videoCassette){
        return orderRepository.save(videoCassette);
    }

    public void delete(Long id){
        orderRepository.deleteById(id);
    }
    @EventListener(ApplicationReadyEvent.class)
    public void fillDB(){
        Save(new Zamowienie(1L, new Customer(1L,"Mariola Paździoch","Peronowa 1 i 3/4"), Collections.singleton(new Product(1L, "Honda papieska", 1000, true)), LocalDate.of(1995,1,1), "Niezrealizowane"));
        Save(new Zamowienie(2L, new Customer(1L,"Mariola Paździoch","Peronowa 1 i 3/4"), Collections.singleton(new Product(1L, "Honda papieska", 1000, true)), LocalDate.of(1990,2,2), "Zrealizowane"));
    }


}
