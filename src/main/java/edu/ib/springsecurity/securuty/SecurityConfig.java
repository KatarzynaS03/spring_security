package edu.ib.springsecurity.securuty;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import javax.sql.DataSource;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    PasswordEncoderConfig passwordEncoderConfig;

    @Autowired
    DataSource dataSource;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication()
                .usersByUsernameQuery("SELECT u.name, u.password_hash, 1 FROM user_dto  u WHERE u.name=?")
                .authoritiesByUsernameQuery("SELECT u.name, u.role, 1 FROM user_dto u WHERE u.name=?")
                .dataSource(dataSource)
                .passwordEncoder(passwordEncoderConfig.passwordEncoder());
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.httpBasic().and().authorizeRequests()
                .antMatchers("/console/**").permitAll()
                .antMatchers(HttpMethod.GET,"/api/product", "/api/product/all", "/api/zamowienia", "/api/zamowienia/all").permitAll()
                .antMatchers(HttpMethod.POST,"/api/zamowienia").permitAll()
                .antMatchers(HttpMethod.GET, "/api/customer", "/api/customer/all").hasAnyRole("CUSTOMER","ADMIN")
                .antMatchers(HttpMethod.POST,"/api").hasAnyRole("CUSTOMER","ADMIN")
                .antMatchers(HttpMethod.POST,"/api/admin/customer", "/api/admin/product").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT,"/api/admin/customer","/api/admin/product", "/api/admin/zamowienia").hasRole("ADMIN")
                .antMatchers(HttpMethod.PATCH,"/api/admin/customer", "/api/admin/zamowienia", "/api/admin/product").hasRole("ADMIN")
                .antMatchers(HttpMethod.DELETE,"/api").hasRole("ADMIN")
                .anyRequest().hasRole("ADMIN");
        http.csrf().disable();
        http.headers().frameOptions().disable();
    }
}
