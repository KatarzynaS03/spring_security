package edu.ib.springsecurity.dto;
import lombok.Data;


@Data
public class CustomerDto {
    private Long id;
    private String name;
    private String addres;
}
