package edu.ib.springsecurity.dto;

import edu.ib.springsecurity.repo.entity.User;
import edu.ib.springsecurity.securuty.PasswordEncoderConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserDtoBuilder {

    @Autowired
    public UserDtoBuilder(PasswordEncoderConfig passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    public UserDtoBuilder() {
    }


    PasswordEncoderConfig passwordEncoder;

    public UserDto buildUser (User user){
        UserDto user2 = new UserDto(user.getName(), passwordEncoder.passwordEncoder().encode(user.getPassword()), user.getRole());
        return user2;
    }

}