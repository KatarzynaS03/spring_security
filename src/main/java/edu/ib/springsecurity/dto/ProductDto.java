package edu.ib.springsecurity.dto;


import lombok.Data;

@Data
public class ProductDto {

    private Long prod_id;
    private String mark;
    private float price;
    private boolean available;
}
