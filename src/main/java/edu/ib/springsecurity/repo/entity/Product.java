package edu.ib.springsecurity.repo.entity;

import javax.persistence.*;

@Entity
@Table(name = "Product")
public class Product{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String mark;

    private float price;

    private boolean available;

    public Product(Long id, String mark, float price, boolean available) {
        this.id = id;
        this.mark = mark;
        this.price = price;
        this.available = available;
    }

    public Product(String mark, float price, boolean available) {
        this.mark = mark;
        this.price = price;
        this.available = available;
    }

    public Product() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", mark='" + mark + '\'' +
                ", price=" + price +
                ", available=" + available +
                '}';
    }
}
