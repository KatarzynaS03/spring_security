package edu.ib.springsecurity.repo.entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;


@Entity
@Table(name = "Zamowienia")
public class Zamowienie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "custmer_id")
    private Customer custmer;

    public Customer getCustmer() {
        return custmer;
    }

    @ManyToMany
    private Set<Product> produkt = new HashSet<Product>();

    private LocalDate placeDate;

    private String status;

    public Long getId() {
        return id;
    }

    public Zamowienie(Long id, Customer custmer, Set<Product> produkt, LocalDate placeDate, String status) {
        this.id = id;
        this.custmer = custmer;
        this.produkt = produkt;
        this.placeDate = placeDate;
        this.status = status;
    }
    public Zamowienie(Customer custmer, Set<Product> produkt, LocalDateTime placeDate, String status) {
        this.custmer = custmer;
        this.produkt = produkt;
        this.placeDate = LocalDate.from(placeDate);
        this.status = status;
    }

    public Zamowienie(){

    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setCustmer(Customer custmer) {
        this.custmer = custmer;
    }

    public Set<Product> getProdukt() {
        return produkt;
    }

    public void setProdukt(Set<Product> produkt) {
        this.produkt = produkt;
    }

    public LocalDate getPlaceDate() {
        return placeDate;
    }

    public void setPlaceDate(LocalDate placeDate) {
        this.placeDate = placeDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Zamowienia{" +
                "id=" + id +
                ", custmer=" + custmer +
                ", produkt=" + produkt +
                ", placeDate=" + placeDate +
                ", status='" + status + '\'' +
                '}';
    }
}
