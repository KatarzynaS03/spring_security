package edu.ib.springsecurity.repo;

import edu.ib.springsecurity.dto.UserDto;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<UserDto, Long> {

    UserDto findByName(@Param("name") String name);

    @Override
    Optional<UserDto> findById(Long aLong);
}