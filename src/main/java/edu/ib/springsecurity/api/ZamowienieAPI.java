package edu.ib.springsecurity.api;

import edu.ib.springsecurity.dto.ZamowienieDto;
import edu.ib.springsecurity.repo.entity.Zamowienie;
import edu.ib.springsecurity.service.ZamowieniaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api")
public class ZamowienieAPI {

    private final ZamowieniaService zamowieniaService;

    @Autowired
    public ZamowienieAPI (ZamowieniaService zamowieniaService) {
        this.zamowieniaService = zamowieniaService;
    }

    @GetMapping("/zamowienia/all")
    public Iterable<Zamowienie> getALL(){

        return zamowieniaService.findALL();
    }
    @GetMapping("/zamowienia")
    public Optional<Zamowienie> getById(@RequestParam Long index){
        return zamowieniaService.FindById(index);
    }

    @PostMapping("/admin/zamowienia")
    public Zamowienie addVIdeo(@RequestBody Zamowienie zamowienie){
        return zamowieniaService.Save(zamowienie);
    }

    @PutMapping("/admin/zamowienia")
    public Zamowienie updateVideo(@RequestBody Zamowienie zamowienie){
        return zamowieniaService.Save(zamowienie);
    }
    @DeleteMapping("/zamowienia")
    public void deleteVideo(@RequestParam Long index){
        zamowieniaService.delete(index);
    }

    @PatchMapping("/admin/zamowienia")
    public void patchProduct(
            @RequestParam Long index,
            @RequestBody ZamowienieDto zamowienieDto)  {
        Zamowienie zamowienie = zamowieniaService
                .FindById(index).orElseThrow(RuntimeException::new);

        boolean needUpdate = false;

        if (zamowienieDto.getCustomer()!= null) {
            zamowienie.setCustmer(zamowienieDto.getCustomer());
            needUpdate = true;
        }

        if (!zamowienieDto.getProduct().isEmpty()) {
            zamowienie.setProdukt(zamowienieDto.getProduct());
            needUpdate = true;
        }

        if (zamowienieDto.getPlaceDate() != null) {
            zamowienie.setPlaceDate(zamowienieDto.getPlaceDate());
            needUpdate = true;
        }

        if (StringUtils.hasLength(zamowienieDto.getStatus())) {
            zamowienie.setStatus(zamowienieDto.getStatus());
            needUpdate = true;
        }

        if (needUpdate) {
            zamowieniaService.Save(zamowienie);
        }
    }

}
