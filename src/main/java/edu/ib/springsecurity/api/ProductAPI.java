package edu.ib.springsecurity.api;

import edu.ib.springsecurity.dto.ProductDto;
import edu.ib.springsecurity.repo.entity.Product;
import edu.ib.springsecurity.service.ProduktService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api")
public class ProductAPI {

    private final ProduktService produktService;

    @Autowired
    public ProductAPI (ProduktService produktService) {
        this.produktService = produktService;
    }

    @GetMapping("/product/all")
    public Iterable<Product> getALL(){

        return produktService.findALL();
    }
    @GetMapping("/product")
    public Optional<Product> getById(@RequestParam Long index){
        return produktService.FindById(index);
    }

    @PostMapping("/admin/product")
    public Product addVIdeo(@RequestBody Product product){
        return produktService.Save(product);
    }

    @PutMapping("/admin/product")
    public Product updateVideo(@RequestBody Product product){
        return produktService.Save(product);
    }
    @DeleteMapping("/product")
    public void deleteVideo(@RequestParam Long index){
        produktService.delete(index);
    }

    @PatchMapping("/admin/product")
    public void patchProduct(
            @RequestParam  Long index,
            @RequestBody ProductDto productDto)  {
        Product product = produktService
                .FindById(index).orElseThrow(RuntimeException::new);

        boolean needUpdate = false;

        if (StringUtils.hasLength(productDto.getMark())) {
            product.setMark(productDto.getMark());
            needUpdate = true;
        }

        if (product.getPrice()>0) {
            product.setPrice(productDto.getPrice());
            needUpdate = true;
        }
        if (StringUtils.hasLength(String.valueOf(productDto.isAvailable()))) {
            product.setAvailable(productDto.isAvailable());
            needUpdate = true;
        }

        if (needUpdate) {
            produktService.save(product);
        }
    }
}
