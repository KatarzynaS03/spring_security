package edu.ib.springsecurity.api;

import edu.ib.springsecurity.dto.CustomerDto;
import edu.ib.springsecurity.repo.entity.Customer;
import edu.ib.springsecurity.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api")
public class CustomerAPI {
    private final CustomerService customerService;

    @Autowired
    public CustomerAPI(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/customer/all")
    public Iterable<Customer> getALL(){

        return customerService.findALL();
    }
    @GetMapping("/customer")
    public Optional<Customer> getById(@RequestParam Long index){
        return customerService.FindById(index);
    }

    @PostMapping("/admin/customer")
    public Customer addVIdeo(@RequestBody Customer customer){
        return customerService.Save(customer);
    }

    @PutMapping("/admin/customer")
    public Customer updateVideo(@RequestBody Customer customer){
        return customerService.Save(customer);
    }

    @PatchMapping("/admin/customer")
    public void patchCustomer(
            @RequestParam Long index,
            @RequestBody CustomerDto customerDto)  {
        Customer customer = customerService
                    .FindById(index).orElseThrow(RuntimeException::new);

            boolean needUpdate = false;

            if (StringUtils.hasLength(customerDto.getName())) {
                customer.setName(customerDto.getName());
                needUpdate = true;
            }

            if (StringUtils.hasLength(customerDto.getAddres())) {
                customer.setAddres(customerDto.getAddres());
                needUpdate = true;
            }
            if (needUpdate) {
                customerService.save(customer);
            }
        }

}
